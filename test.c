#include<stdio.h>
#include<stdlib.h>

int ysize;
void yfree(void *ptr);
void *ymalloc(int size);
void block(int size);

void *yalloc;


int main(void){
	printf("\nEnter the size of block you want : ");
	scanf("%d",&ysize);
	block(ysize);
	
	int *a = (int *)ymalloc(sizeof(int)*3);

	printf("\nEnter 3 elements in an array(a) : ");
	for(int i=0 ; i<3 ; i++){
		scanf("%d",&a[i]);
	}
	printf("\nStart Address=%ld End Address=%ld",(long int)a,(long int)(a+3));
	printf("\nYour Entered Array(a) : ");
	for(int i=0 ; i<3 ; i++){
		printf("%d ",a[i]);
	}
	int *b = (int *)ymalloc(sizeof(int)*3);

	printf("\nEnter 3 elements in an array(b): ");
	for(int i=0 ; i<3 ; i++){
		scanf("%d",&b[i]);
	}
	printf("\nStart Address=%ld End Address=%ld",(long int)b,(long int)(b+3));
	printf("\nYour Entered Array(b) : ");
	for(int i=0 ; i<3 ; i++){
		printf("%d ",b[i]);
	}
	yfree(b);
	int *c = (int *)ymalloc(sizeof(int)*3);

	printf("\nEnter 3 elements in an array(c): ");
	for(int i=0 ; i<3 ; i++){
		scanf("%d",&c[i]);
	}
	printf("\nStart Address=%ld End Address=%ld",(long int)c,(long int)(c+3));
	printf("\nYour Entered Array(b) : ");
	for(int i=0 ; i<3 ; i++){
		printf("%d ",c[i]);
	}
	yfree(a);
	yfree(c);
	printf("\n\n");
}
