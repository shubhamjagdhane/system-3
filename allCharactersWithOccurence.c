#include<stdio.h>
#include<string.h>
#include<stdlib.h>

typedef struct RecordsOfAllVariables{
	int tcnt,totc;
	char vname;
}Records;

Records Node[256];

void displayAllVariablesWithIndex(int tot){
	printf("\n\t\t\t****All characters in given file****\n\n");
	for(int k=0 ; k<tot ; k++)
		printf("\ncharacter name = %c\toccurrence = %d\tASCII=%d",Node[k].vname,Node[k].totc,Node[k].vname);

}

int readFile(char fname[]){
	printf("Filename = %s",fname);
	FILE *fp = fopen(fname,"r");
	int i=0,j=0,flag=0,id=0;
	char ch;
	ch=fgetc(fp);
	while(ch!=EOF){
		for(i=0 ; i<id ; i++){
			if(Node[i].vname==ch){
				flag=1;
				break;
			}
		}
		if(flag==0){
			Node[id].vname=ch;
			Node[id].tcnt=id;
			Node[id].totc=1;
			id++;
		}
		else{
			Node[i].totc+=1;
			flag=0;
		}

		ch=fgetc(fp);
	}
	return id;
}

void descendingOrder(int tot){
	int i,j,tempi;
	char tempc;
	for(i=0 ; i<tot ; i++){
		for(j=0 ; j<tot; j++){
			if(Node[i].totc>Node[j].totc){
				tempi=Node[i].totc;
				tempc=Node[i].vname;

				Node[i].totc=Node[j].totc;
				Node[i].vname=Node[j].vname;

				Node[j].totc=tempi;
				Node[j].vname=tempc;
			}
		}
	}
}

int main(int argc,char *argv[]){
	int td,md;
	if(argc!=2){
		printf("\nPlease pass argument as a command line..!!");
		exit(0);
	}
	td=readFile(argv[1]);
	displayAllVariablesWithIndex(td);	

	printf("\n\n");
	
	descendingOrder(td);
	printf("\n\t\t****After Sorting****\n\n");
	displayAllVariablesWithIndex(td);
	printf("\n\n");
	printf("\t\t****Total Characters=%d****\n\n",td);
}
