#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/types.h>
#include<fcntl.h>
#include<unistd.h>
#include<sys/stat.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<netinet/in.h>
#define PORT 5000
//#define SIZE 80
int arrid[1000],i;
typedef struct ServerData{
	int flag,id;
	char fname[1024];
}Server;

int main(void){
	int SIZE = (sizeof(Server));
	int sockfd,len,n;
	int ifd,ofd,cnt=0;
	struct stat st;
	ssize_t cin,cout;	
	struct sockaddr_in servaddr,cliaddr;
	sockfd = socket(AF_INET,SOCK_DGRAM,0);
	if(sockfd<0){
		perror("Error");
		exit(0);
	}
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = INADDR_ANY;
	servaddr.sin_port = htons(PORT);
	
	if((bind(sockfd,(struct sockaddr*)&servaddr,sizeof(servaddr)))<0){
			perror("bind");
			exit(0);
	}
	Server *Node=NULL;
	Node = (Server *)malloc(sizeof(Server));
	while(1){
		len = sizeof(cliaddr);
		n = recvfrom(sockfd,Node,SIZE,0,(struct sockaddr *)&cliaddr,&len);
//Work on above recvfrom so that it can take multiple clients request at a time..!!
		if(Node->flag==0){
			ifd=open(Node->fname,O_RDONLY);
			if(ifd==-1){
				printf("\nFile not found\n");
				Node->flag=0;
				sendto(sockfd,Node,SIZE,0,(struct sockaddr*)&cliaddr,len);
			}
			else{
				printf("\n**Server is ON for %s file...!!**",Node->fname);
				stat(Node->fname,&st);
				Node->flag=st.st_size;
				arrid[i]=ifd;
				Node->id=arrid[i];
				i++;	
				sendto(sockfd,Node,SIZE,0,(struct sockaddr*)&cliaddr,len);
			}
		}
		else{
				cin = read(Node->id,Node->fname,1024);
				cin = cin+8;
				sendto(sockfd,Node,cin,0,(struct sockaddr*)&cliaddr,len);
			if(cin==0){
				close(Node->id);
			}
			cnt++;
		}	
	}
	return 0;
}
