#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<netinet/in.h>
#define PORT 5000
#define SIZE 80

int token=0,i=0;

typedef struct ServerStruct{
	int cid,rcid,flag;
	char msg[1024];
}Server;

int main(void){
	int sockfd,len,n;
	char *buffer;
	buffer = (char *)malloc(80);
	struct sockaddr_in servaddr,cliaddr;
	sockfd = socket(AF_INET,SOCK_DGRAM,0);
	if(sockfd<0){
		perror("Error");
		exit(0);
	}
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = INADDR_ANY;
	servaddr.sin_port = htons(PORT);
	
	if((bind(sockfd,(struct sockaddr*)&servaddr,sizeof(servaddr)))<0){
			perror("bind");
			exit(0);
	}
	Server Node[10],*demo;
	demo = (Server *)malloc(sizeof(Server));
	while(1){
		len = sizeof(cliaddr);
		n = recvfrom(sockfd,demo,(sizeof(Server)),0,(struct sockaddr *)&cliaddr,&len);
		if(demo->flag==0){
			Node[token].cid=token+1;
			Node[token].rcid=demo->rcid;
			Node[token].flag=1;
			strcpy(Node[token].msg,demo->msg);
			demo->flag=1;
			demo->cid=Node[token].cid;
			token++;
			sendto(sockfd,demo,(sizeof(Server)),0,(struct sockaddr*)&cliaddr,len);
		}

		while(i!=token){
			printf("\ni=%d\nNode->cid=%d\nNode->rcid=%d\nNode->flag=%d\nNode->msg=%s\n",i,Node[i].cid,Node[i].rcid,Node[i].flag,Node[i].msg);
			
			demo->cid=Node[i].cid;
			demo->rcid=Node[i].rcid;
			demo->flag=Node[i].flag;
			strcpy(demo->msg,Node[i].msg);	
			i++;
			sendto(sockfd,demo,(sizeof(Server)),0,(struct sockaddr*)&cliaddr,len);
		}
		i=0;
	}
}
