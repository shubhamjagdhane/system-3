#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<fcntl.h>
#include<unistd.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<netinet/in.h>
#define PORT 5000
#define SIZE 1024

void revstr(char *str,int l){
	int i,j;
	char temp;
	for(i=0 , j=l-1 ; i<j ; i++,j--){
		temp = str[i];
		str[i]=str[j];
		str[j]=temp;
	}
}


int main(void){
	int ifd,ofd,cnt=0;
	char *list,*fstr;
	struct stat st;
	list = (char *)malloc(SIZE);
	fstr = (char *)malloc(100);
	ssize_t cin,cout;
	unsigned long int tfsize,fsize,tots=0;

	int sockfd,len,n;
	char *buffer;
	buffer = (char *)malloc(SIZE);
	struct sockaddr_in servaddr,cliaddr;
	sockfd = socket(AF_INET,SOCK_DGRAM,0);
	if(sockfd<0){
		perror("Error");
		exit(0);
	}
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = INADDR_ANY;
	servaddr.sin_port = htons(PORT);
	
	if((bind(sockfd,(struct sockaddr*)&servaddr,sizeof(servaddr)))<0){
			perror("bind");
			exit(0);
	}

	len = sizeof(cliaddr);
	n = recvfrom(sockfd,buffer,SIZE,0,(struct sockaddr *)&cliaddr,&len);
	printf("\n\t***Client is requesting for %s filename***\n",buffer);
	ifd = open (buffer,O_RDONLY);
	if(ifd==-1){
		printf("\nError open for reading\n");
		return -1;
	}
/*	ofd = open(buffer,O_WRONLY | O_CREAT,0644);
	if(ofd == -1){
		printf("\nError open for writing\n");
	return -1;
	}*/
	stat(buffer,&st);
	fsize = st.st_size;
	tfsize=fsize;
	for(int i=0 ; fsize>0 ; i++){
		fstr[i] = (fsize%10)+48 ;
		fsize = fsize/10;	
	}
	int l=strlen(fstr);
	printf("\nLength = %d\n",l);
	revstr(fstr,l);
	printf("fstr=%s\n",fstr);

	sendto(sockfd,fstr,l,0,(struct sockaddr*)&cliaddr,len);


	n = recvfrom(sockfd,buffer,SIZE,0,(struct sockaddr *)&cliaddr,&len);
	printf("buffer = %s\n",buffer);
	cin = read(ifd,list,1024);
	tots = tots+cin;
	cnt++;
	buffer=" ";
	while(cin>0){
		sendto(sockfd,list,cin,0,(struct sockaddr*)&cliaddr,len);
		n = recvfrom(sockfd,list,SIZE,0,(struct sockaddr *)&cliaddr,&len);
		if(list[0]=='G'){
			break;
		}
		printf("cin = %ld ",cin);
		tots=tots+cin;
		cnt++;
		cin = read(ifd,list,1024);

	}
	printf("counter = %d\n",cnt);
	close(ifd);

	return 0;
}
