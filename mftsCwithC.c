#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/types.h>
#include<fcntl.h>
#include<unistd.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<netinet/in.h>

#define PORT 5000
#define SIZE 1032

typedef struct ClientData{
	int flag,id;
	char fname[1024];
}Client;

int main(int argc,char **argv){
	if(argc!=2){
		printf("\nPlease pass the filename along with executable.\n");
		exit(0);
	}
	int ofd,tots=0,cnt=0;
//	int SIZE=(sizeof(Client));
	ssize_t cout;
	int sockfd,len,n,flag=0,totsize,cid;
	char *buffer,*sendBuffer;
	buffer = (char *)malloc(SIZE);
	sendBuffer = (char *)malloc(SIZE);
	Client *Node=NULL;
	
	struct sockaddr_in servaddr;
	sockfd = socket(AF_INET,SOCK_DGRAM,0);

	if(sockfd<0){
		perror("Socket");
		exit(0);
	}

	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = INADDR_ANY;
	servaddr.sin_port = htons(PORT);

	len = sizeof(servaddr);
/*	if(Node==NULL){
		printf("\nNode is NULL in client\n");		
	}*/
	Node = (Client *)malloc(sizeof(Client));
	Node->flag=0;
	strcpy(Node->fname,argv[1]);
//	printf("\nFlag : %d and filename = %s",Node->flag,Node->fname);
	sendto(sockfd,Node,SIZE,0,(const struct sockaddr*)&servaddr,len);
	n = recvfrom(sockfd,Node,SIZE,0,(struct sockaddr *)&servaddr,&len);
	totsize = Node->flag;
	cid = Node->id;
	if(Node->flag>0){
		printf("\nYour (%d)Bytes amount of memory is going to used...!!!\n",totsize);
		Node->flag=1;
		ofd = open(Node->fname,O_WRONLY | O_CREAT,0644);
		if(ofd==-1){
			printf("\nError open for writing\n");
			return -1;
		}
	}
	else{
		printf("\nServer does not have %s file\nTry another file name\n",Node->fname);
		exit(0);
	}
	n=1024;
	sendto(sockfd,Node,SIZE,0,(const struct sockaddr*)&servaddr,len);
	while(totsize!=tots){
		cnt++;
		n = recvfrom(sockfd,Node,SIZE,0,(struct sockaddr *)&servaddr,&len);
		if(Node->flag==0){
			printf("\nServer does not having this file...!!");
			exit(0);
		}
		else{
			n=n-8;
			cout = write(ofd,Node->fname,(ssize_t)n);
			tots = tots+n;
			printf("\rFile is Downloading...!!!Completing (%d)Bytes of (%d)Bytes",tots,totsize);
			fflush(stdout);
			if(totsize==tots){
				strcpy(Node->fname,"stop");
				sendto(sockfd,Node,SIZE,0,(const struct sockaddr*)&servaddr,len);
				printf("\nYou got a new file\n");
				break;
			}
			sendto(sockfd,Node,SIZE,0,(const struct sockaddr*)&servaddr,len);
		}
	}
//	printf("\ntotsize=%d\ntots=%d",totsize,tots);
//	printf("\ncounter = %d",cnt);
	close(ofd);
	printf("\n\n");
	return 0;
}
