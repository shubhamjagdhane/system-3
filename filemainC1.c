#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<fcntl.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<netinet/in.h>

#define PORT 5000
#define SIZE 1024

int main(int argc,char **argv){
	if(argc!=2){
		printf("\nUsage : client <filename>\n");
		return -1;
	}
	
	int ifd,ofd;
	ssize_t cin,cout;
	char *list;
	
	list = (char *)malloc(SIZE);

	int sockfd,len,n=1024,cnt=0;
	unsigned long int ogvalue,tots=0;
	char *buffer,*sendBuffer;
	buffer = (char *)malloc(SIZE);
	sendBuffer = (char *)malloc(SIZE);
	
	struct sockaddr_in servaddr;
	sockfd = socket(AF_INET,SOCK_DGRAM,0);

	if(sockfd<0){
		perror("Socket");
		exit(0);
	}

	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = INADDR_ANY;
	servaddr.sin_port = htons(PORT);

	len = sizeof(servaddr);
	sendBuffer = argv[1];
	sendto(sockfd,sendBuffer,SIZE,0,(const struct sockaddr*)&servaddr,len);


	n = recvfrom(sockfd,buffer,SIZE,0,(struct sockaddr *)&servaddr,&len);

	printf("String = %s\n",buffer);
	ogvalue = atoi(buffer);
	printf("Number = %ld\n",ogvalue);

	sendto(sockfd,"Send File",10,0,(const struct sockaddr*)&servaddr,len);

	ofd = open(sendBuffer,O_WRONLY | O_CREAT,0644);
	if(ofd==-1){
		printf("\nError open for writing\n");
		return -1;
	}
	n=1024;
	while(ogvalue!=tots && n>0){
		cnt++;
		n = recvfrom(sockfd,list,SIZE,0,(struct sockaddr *)&servaddr,&len);
		printf("n=%d ",n);
		printf("cnt=%d ",cnt);

		tots=tots+n;
		printf("tots=%ld ",tots);
		printf("ogvalue=%ld ",ogvalue);
		cout = write(ofd,list,(ssize_t)n);
		if(tots==ogvalue){
	sendto(sockfd,"G",10,0,(const struct sockaddr*)&servaddr,len);
		}

	sendto(sockfd,"P",10,0,(const struct sockaddr*)&servaddr,len);
	}

	printf("\ncounter=%d\n",cnt);
	close(ofd);
	printf("tots=%ld\n",tots);
	return 0;
}
