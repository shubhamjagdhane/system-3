#include<stdio.h>
#include<stdlib.h>

int *arr1,*arr2;

typedef struct UnallocatedList{
//	struct UnallocatedList *start,*end,*next;
	long int start,end;
	struct UnallocatedList *next;
}unalloc;

unalloc *uhead=NULL;
int aend=0,astart=0,totsize,flag=0,i=0;
void *yalloc;
void mergeL();
void yfree(void *ptr);

void adisplay(void){

	printf("\n\t\t***Allocated List***");
	printf("\nArray 1 : ");
	for(int j=0 ; j<i ; j++){
		printf("%d ",arr1[j]);
	}
	printf("\nArray 2 : ");
	for(int j=0 ; j<i ; j++){
		printf("%d ",arr2[j]);
	}

}

void udisplay(void){
	unalloc *p=uhead;
	printf("\n\t\t***Unallocated List***");
	while(p!=NULL){
		printf("\nStart Address = %ld End Address = %ld",(long int)p->start,(long int)p->end);
		p = p->next;
	}
	adisplay();
	printf("\n\n");
}

void updateUList(void *end){
	if(uhead!=NULL){
		unalloc *p=uhead;
		p->start =(long int)end;
		udisplay();
	}
	else{
		return;
	}
}

void *ymalloc(int size){
	aend = aend+size;
	if(aend>totsize){
		printf("\n\n***ERROR : You have only (%d)bytes of memory and\n\t        you want (%d)bytes of memory***",totsize,aend);
		printf("\n\nSegmentation Fault..!!\n");
		exit(0);
	}
	else{
		astart = aend-size;
		if(flag==1){
			arr1[i]=(long int)(yalloc+astart);
			arr2[i]=size;
//			adisplay();
			i++;
		}
		updateUList(yalloc+aend);
		return (yalloc+astart);
	}
}

void unallocList(int size){
	unalloc *newNode=(unalloc *)ymalloc(sizeof(unalloc));
	newNode->start=(long int)(yalloc);
	newNode->end = (long int)(yalloc+size);
	newNode->next=NULL;
	if(uhead==NULL){
		uhead = newNode;
	}
	udisplay();
}


void block(int size){
	totsize=size;
	yalloc = malloc(size);
	printf("\n\t\t**Notice:You are allow to used only addresses between**\n\t\tStart Address = %ld and End Address = %ld",(long int)yalloc,(long int)(yalloc+size));


	arr1 = (int *)ymalloc(sizeof(int)*10);
	arr2 = (int *)ymalloc(sizeof(int)*10);
	printf("\nArray 1 size = %ld\tArray 2 size = %ld",sizeof(arr1),sizeof(arr2));
	unallocList(size);
	flag=1;
}

int removeFromAllocated(void *ptr){
	int j=0,flag=0;
	long int ch=(long int)ptr;
	printf("\n\n***Deleting address = %ld\n\n",ch);
	while(j<i){
		if(arr1[j]==ch){
			flag=1;
			break;
		}
		j++;
	}
	if(flag==1){
		return j;
	}
	else{
		return -1;
	}
}

void insertIntoUnallocated(int start,int end){
	unalloc *p=NULL;
	unalloc *newNode = (unalloc *)ymalloc(sizeof(unalloc));
	newNode->start=(long int)start;
	newNode->end = (long int)(start+end);
	newNode->next=NULL;
	if(uhead==NULL){
		uhead = newNode;
	}
	else{
		p=uhead;
		while(p->next!=NULL){
			p = p->next;	
		}
		p->next = newNode;
	}
}

void mergeL(){
	unalloc *p=uhead;
    unalloc *q=p,*r=p;
    while(p!=NULL && q!=NULL){
    	if(q->end==(p->start)){
        	q->end = p->end;
            r->next = p->next;
            p->next = NULL;
            yfree(p);
            r=q;
            p=q;
		}
        if((q->start) == (p->end)){
        	q->start = p->start;
            r->next = p->next;
            p->next = NULL;
            yfree(p);
            r=q;
            p=q;
		}
        r = p;
        p = p->next;
        if(p==NULL){
        	p=q;
            q=q->next;
        }
	}
}

void yfree(void *ptr){
	int j;
	j=removeFromAllocated(ptr);
	printf("\n\n***value of j = %d",j);
	printf("\n\nIn Yfree=> arr1[j]=%d and arr2[j]=%d***\n\n",arr1[j],arr2[j]);
	if(j!=-1){
		insertIntoUnallocated(arr1[j],arr2[j]);
		for(int k=j ; k<i-1 ; k++){
			arr1[k]=arr1[k+1];
			arr2[k]=arr2[k+1];
		}
		i = i-1;
		mergeL();
		udisplay();
		adisplay();
	}
	else{
		printf("\nERROR : You are tring to free that memory which is not allocated..!!");
		exit(0);
	}
}


