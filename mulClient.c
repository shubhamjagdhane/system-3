#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<netinet/in.h>

#define PORT 5000
#define SIZE 80
typedef struct ClientStruct{
	int flag,id;
	char msg[1024];
}Client;

int main(void){
	int sockfd,len,n;
	char *buffer,*sendBuffer;
	buffer = (char *)malloc(SIZE);
	sendBuffer = (char *)malloc(SIZE);
	
	struct sockaddr_in servaddr;
	sockfd = socket(AF_INET,SOCK_DGRAM,0);

	if(sockfd<0){
		perror("Socket");
		exit(0);
	}

	servaddr.sin_family = AF_INET;
//	servaddr.sin_addr.s_addr = INADDR_ANY;
/*	if(inet_pton(AF_INET,"127.0.1",&servaddr.sin_addr)<0){
		printf("Invalid Address");
		return -1;
	}*/
	servaddr.sin_port = htons(PORT);
	Client *Node = (Client *)malloc(sizeof(Client));
	len = sizeof(servaddr);
	Node->flag=0;

	sendto(sockfd,Node,(sizeof(Client)),0,(const struct sockaddr*)&servaddr,len);

	n = recvfrom(sockfd,Node,(sizeof(Client)),0,(struct sockaddr *)&servaddr,&len);
	printf("\nClient Id = %d\n",Node->id);
//	while(fgets(sendBuffer,SIZE,stdin)!=NULL){
//		sendto(sockfd,Node,(sizeof(Client)),0,(const struct sockaddr*)&servaddr,len);
	sprintf(buffer,"%d",Node->id);
	if(inet_pton(AF_INET,buffer,&servaddr.sin_addr)<0){
		printf("Invalid Address");
		return -1;
	}
//		n = recvfrom(sockfd,Node,SIZE,0,(struct sockaddr *)&servaddr,&len);
		if(strcmp(buffer,"1")==0){
			printf("\n\t\t***Hi I am client 1***\n\n");
		}
		else{
			printf("\n\t\t***You are not client 1***\n\n");
		}
//	}
	return 0;
}
