#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<netinet/in.h>
#define PORT 5000
#define SIZE 80

typedef struct ServerNode{
	int flag,id;
	char msg[1024];
}Server;

int token=0;

int main(void){
	int sockfd,len,n;
	char *buffer;
	buffer = (char *)malloc(80);
	struct sockaddr_in servaddr,cliaddr;
	sockfd = socket(AF_INET,SOCK_DGRAM,0);
	if(sockfd<0){
		perror("Error");
		exit(0);
	}
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = INADDR_ANY;
	servaddr.sin_port = htons(PORT);
	
	if((bind(sockfd,(struct sockaddr*)&servaddr,sizeof(servaddr)))<0){
			perror("bind");
			exit(0);
	}
	token++;
	Server *Node=(Server *)malloc(sizeof(Server));
	while(1){
		len = sizeof(cliaddr);
		n = recvfrom(sockfd,Node,(sizeof(Server)),0,(struct sockaddr *)&cliaddr,&len);
		if(Node->flag==0){
			Node->flag=1;
			Node->id = token;
			token++;
			sendto(sockfd,Node,(sizeof(Server)),0,(struct sockaddr*)&cliaddr,len);
		}
		else{
			sendto(sockfd,Node,(sizeof(Server)),0,(struct sockaddr*)&cliaddr,len);
		}
	
}
	return 0;
}
