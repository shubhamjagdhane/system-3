#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<netinet/in.h>

#define PORT 5000
#define SIZE 80

typedef struct ClientStruct{
	int cid,rcid,flag;
	char msg[1024];
}Client;

int i=0;

int main(int argc,char *argv[]){
	if(argc!=3){
		printf("\nPlease pass arguement as command line..!!");
		exit(0);
	}
	int sockfd,len,n;
	char *buffer,*sendBuffer;
	buffer = (char *)malloc(SIZE);
	sendBuffer = (char *)malloc(SIZE);
	
	struct sockaddr_in servaddr;
	sockfd = socket(AF_INET,SOCK_DGRAM,0);

	if(sockfd<0){
		perror("Socket");
		exit(0);
	}

	servaddr.sin_family = AF_INET;
//	servaddr.sin_addr.s_addr = INADDR_ANY;
	if(inet_pton(AF_INET,"127.0.0.1",&servaddr.sin_addr)<0){
		printf("Invalid Address");
		return -1;
	}
	servaddr.sin_port = htons(PORT);

	Client *Node = (Client *)malloc(sizeof(Client));
	int unid;
	Node->cid=-1;
	Node->flag=0;
	Node->rcid=atoi(argv[1]);
	strcpy(Node->msg,argv[2]);
	len = sizeof(servaddr);
	sendto(sockfd,Node,(sizeof(Client)),0,(const struct sockaddr*)&servaddr,len);
	n = recvfrom(sockfd,Node,(sizeof(Client)),0,(struct sockaddr *)&servaddr,&len);
	unid=Node->cid;
	printf("\n\nClient Id = %d",unid);
	printf("\ncid=%d\nrcid=%d\nmsg=%s\n",Node->cid,Node->rcid,Node->msg);
	while(1){
		sendto(sockfd,Node,(sizeof(Client)),0,(const struct sockaddr*)&servaddr,len);
		n = recvfrom(sockfd,Node,(sizeof(Client)),0,(struct sockaddr *)&servaddr,&len);
		if(unid==Node->rcid){
			printf("\nClient %d sent you message=%s",Node->cid,Node->msg);
//			break;
		}
	}
	return 0;
}
